let

  pkgs = import ./config/nixpkgs.nix ;

  native = pkgs.haskell.packages.ghc.callCabal2nix "haskell-invaders" ./. {};
  web = pkgs.haskell.packages.ghcjs.callCabal2nix "haskell-invaders" ./. {};

in

  {
    native = native.env;
    web = web.env;
  }

