# haskell-invaders 

A space-invaders-like video game, in Haskell.
[Play online!](https://juliendehos.gitlab.io/haskell-invaders)

- Native version using [gloss](http://hackage.haskell.org/package/gloss):

![](doc/haskell-invaders-native.mp4)

- Web version using [shine](https://hackage.haskell.org/package/shine):

![](doc/haskell-invaders-web.mp4)


## Gameplay

- move paddle: left/right arrows
- fire bullet: space
- exit (native version only): esc


## Build using Nix

- install [Nix](https://nixos.org/nix/)

- build the project: `nix-build config/release.nix`

- run native version: `./result/bin/haskell-invaders-native`

- run web version: `firefox result-2/index.html`


## Build using Stack (native version only)

```
stack build
stack exec haskell-invaders-native
```


## Development environment (using Nix)

```
nix-shell -A native
make native
```

```
nix-shell -A web
make web
firefox static/index.html
```

