let

  pkgs = import ./nixpkgs.nix ;

  haskell-invaders-src = ../.;

  native = pkgs.haskell.packages.ghc.callCabal2nix "haskell-invaders" haskell-invaders-src {};
  web = pkgs.haskell.packages.ghcjs.callCabal2nix "haskell-invaders" haskell-invaders-src {};

in

  {

    inherit native;

    web = pkgs.runCommand "haskell-invaders" { inherit web; } ''
      mkdir -p $out
      ${pkgs.closurecompiler}/bin/closure-compiler ${web}/bin/haskell-invaders-web.jsexe/all.js > $out/all.js
      cp ${haskell-invaders-src}/static/* $out/
    '';

  }

