let

  bootstrap = import <nixpkgs> {};

  nixpkgs-src = bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo  = "nixpkgs";
    rev = "725b5499b89fe80d7cfbb00bd3c140a73cbdd97f";
    sha256 = "0xdhv9k0nq8d91qdw66d6ln2jsqc9ij7r24l9jnv4c4bfpl4ayy7";
  };

  ghcjs-dom-src = bootstrap.fetchFromGitHub {
    owner = "ghcjs";
    repo = "ghcjs-dom";
    rev = "0.9.4.0";
    sha256 = "06qlbbhjd0mlv5cymp5q0rb69a333l0fcif5zwa83h94dh25c1g7";
  };

  config = { 
    packageOverrides = pkgs: rec {
      haskell = pkgs.haskell // {
        packages = pkgs.haskell.packages // {

          ghc = pkgs.haskell.packages.ghc864;

          } // {

          # Many packages don't build on ghcjs because of a dependency on doctest
          # (which doesn't build), or because of a runtime error during the test run.
          # See: https://github.com/ghcjs/ghcjs/issues/711
          ghcjs = pkgs.haskell.packages.ghcjs86.override {
            overrides = self: super: with pkgs.haskell.lib; {
              tasty-quickcheck = dontCheck super.tasty-quickcheck;
              http-types       = dontCheck super.http-types;
              comonad          = dontCheck super.comonad;
              semigroupoids    = dontCheck super.semigroupoids;
              lens             = dontCheck super.lens;
              QuickCheck       = dontCheck super.QuickCheck;
              half             = dontCheck super.half;

              ghcjs-dom-jsffi = self.callCabal2nix "ghcjs-dom-jsffi" (ghcjs-dom-src + "/ghcjs-dom-jsffi") {};
              ghcjs-dom-jsaddle = self.callCabal2nix "ghcjs-dom-jsaddle" (ghcjs-dom-src + "/ghcjs-dom-jsaddle") {};
              ghcjs-dom = self.callCabal2nix "ghcjs-dom" (ghcjs-dom-src + "/ghcjs-dom") {};

              shine = doJailbreak (super.shine.overrideDerivation (attrs: {
                preConfigure = "sed -i \"s/2.7/2.8/\" shine.cabal";
              }));

            };
          };

        };
      };
    };
  };

in

  import nixpkgs-src { inherit config; }

