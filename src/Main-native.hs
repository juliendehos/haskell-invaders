import Game
import qualified Graphics.Gloss as G
import qualified Graphics.Gloss.Interface.IO.Interact as G

import System.Random (newStdGen, randoms)

displayH :: Game Float -> G.Picture
displayH g = case _status g of
    Won -> G.Color G.white $ G.Translate (-170) 0 $ G.Scale 0.5 0.5 $ G.Text "YOU WIN!"
    Lost -> G.Color G.white $ G.Translate (-200) 0 $ G.Scale 0.5 0.5 $ G.Text "GAME OVER!"
    _ -> G.pictures $ paddle : bullets ++ invaders
    where paddle = drawItem G.white (_paddle g)
          bullets = map (drawItem G.yellow) (_bullets g)
          invaders = map (drawItem G.red) (_invaders g)

drawItem :: G.Color -> Item Float -> G.Picture
drawItem c it = G.Color c $ G.Translate x y $ G.rectangleSolid sx sy
    where (x, y) = _pos it
          (sx, sy) = _siz it

eventH :: G.Event -> Game Float -> Game Float
eventH (G.EventKey (G.SpecialKey G.KeyLeft) G.Down _ _)  g = g { _inputLeft = True }
eventH (G.EventKey (G.SpecialKey G.KeyLeft) G.Up _ _)    g = g { _inputLeft = False }
eventH (G.EventKey (G.SpecialKey G.KeyRight) G.Down _ _) g = g { _inputRight = True }
eventH (G.EventKey (G.SpecialKey G.KeyRight) G.Up _ _)   g = g { _inputRight = False }
eventH (G.EventKey (G.SpecialKey G.KeySpace) G.Down _ _) g = g { _inputFire = True }
eventH (G.EventKey (G.SpecialKey G.KeySpace) G.Up _ _)   g = g { _inputFire = False }
eventH _ g = g

idleH :: Float -> Game Float -> Game Float
idleH = step

main :: IO ()
main = do
    myRands <- randoms <$> newStdGen
    let myGame = createGame myRands
    let myWindow = G.InWindow "haskell-invaders" (gameWidth, gameHeight) (0, 0)
    G.play myWindow G.black 30 myGame displayH eventH idleH

