
# build web version, in a "nix-shell -A web"
web:
	cabal --builddir=dist-web --config-file=config/web.config build
	mkdir -p static
	ln -sf ../dist-web/build/haskell-invaders-web/haskell-invaders-web.jsexe/all.js static/
	@echo "Done. You can open static/index.html in a browser."

# run native version, in a "nix-shell -A native"
native:
	cabal --builddir=dist-native --config-file=config/native.config run

clean:
	rm -rf dist-* static/all.js

